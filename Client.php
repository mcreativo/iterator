<?php

$collection = new Collection();
$collection
    ->addElement('a')
    ->addElement('b')
    ->addElement('c')
    ->addElement('d');

$iterator = $collection->getElements();

foreach ($iterator as $key => $value) {
    var_dump($key, $value);
}