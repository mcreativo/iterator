<?php


class Collection
{

    /**
     * @var string[]
     */
    private $elements = [];

    function __construct()
    {
    }

    public function addElement($string)
    {
        $this->elements[] = $string;
        return $this;
    }

    public function removeElement($string)
    {
        if (isset($this->elements[$string]))
            unset($this->elements[$string]);
    }

    /**
     * @return \string[]
     */
    public function getElements()
    {
        return $this->elements;
    }

    public function getCount()
    {
        return count($this->$elements);
    }

    public function createIterator()
    {
        return new ConcreteIterator($this->$elements);
    }


} 